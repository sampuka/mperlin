#include "Perlin.hpp"

#include <iostream>

int main()
{
    matt::Perlin pn;

    double max = 0;
    double min = 1;

    double res;
    double inp = 0;

    while (true)
    {
	res = pn.noise(inp, inp);

	if (res > max)
	{
	    max = res;
	    std::cout << '[' << min << ", " << max << ']' << std::endl;
	}
	else if (res < min)
	{
	    min = res;
	    std::cout << '[' << min << ", " << max << ']' << std::endl;
	}
	
	inp += 0.001;
    }

    return 0;
}
