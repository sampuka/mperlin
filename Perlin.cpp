#include "Perlin.hpp"

#include <algorithm>
#include <numeric>
#include <iostream>

matt::Perlin::Perlin()
{
    std::random_device rt;
    std::mt19937 eng(rt());

    std::iota(p.begin(), p.begin()+256, 0);

    std::shuffle(p.begin(), p.begin()+256, eng);

    std::copy(p.begin(), p.begin()+256, p.begin()+256);
}

matt::Perlin::Perlin(std::uint32_t seed)
{
    std::mt19937 eng(seed);
    
    std::iota(p.begin(), p.begin()+256, 0);

    std::shuffle(p.begin(), p.begin()+256, eng);

    std::copy(p.begin(), p.begin()+256, p.begin()+256);
}

double matt::Perlin::noise(double x, double y, double z)
{
    std::uint8_t X = (std::uint8_t) floor(x) & 255;
    std::uint8_t Y = (std::uint8_t) floor(y) & 255;
    std::uint8_t Z = (std::uint8_t) floor(z) & 255;
    
    x -= floor(x);
    y -= floor(y);
    z -= floor(z);
    
    double u = fade(x);
    double v = fade(y);
    double w = fade(z);

    std::uint16_t A = p[X] + Y;
    std::uint16_t AA = p[A] + Z;
    std::uint16_t AB = p[A + 1] + Z;
    std::uint16_t B = p[X + 1] + Y;
    std::uint16_t BA = p[B] + Z;
    std::uint16_t BB = p[B + 1] + Z;
    
    double res = lerp(w, lerp(v, lerp(u, grad(p[AA], x, y, z), grad(p[BA], x-1, y, z)), lerp(u, grad(p[AB], x, y-1, z), grad(p[BB], x-1, y-1, z))), lerp(v, lerp(u, grad(p[AA+1], x, y, z-1), grad(p[BA+1], x-1, y, z-1)), lerp(u, grad(p[AB+1], x, y-1, z-1),	grad(p[BB+1], x-1, y-1, z-1))));
    
    return (res+1.0)/2.0;
}

double matt::Perlin::fade(double t)
{
    return t * t * t * (t * (t * 6 - 15) + 10);
}

double matt::Perlin::lerp(double t, double a, double b)
{
    return a + t * (b - a);
}

double matt::Perlin::grad(std::uint8_t hash, double x, double y, double z)
{
    std::uint8_t h = hash & 15;

    double u = h < 8 ? x : y;
    double v = h < 4 ? y : h == 12 || h == 14 ? x : z;

    return ((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v);
}
