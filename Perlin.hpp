#ifndef PERLIN_H_
#define PERLIN_H_

#include <array>
#include <cstdint>
#include <random>

namespace matt
{
    class Perlin
    {
    public:
	Perlin(); // The class makes a random seed itself
	Perlin(std::uint32_t seed); // Uses this seed instead

	double noise(double x, double y, double z=0); // Read noise

    private:
	double fade(double t);
	double lerp(double t, double a, double b);
	double grad(std::uint8_t hash, double x, double y, double z);

	std::array<std::uint8_t, 512> p; // Permutation vector
    };
}
	
#endif
