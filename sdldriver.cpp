#include "Perlin.hpp"

#include "SDL.h"

#include <iostream>
#include <cmath>

int main()
{
    if (SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO) != 0)
    {
        SDL_Log("Unable to initialize SDL: %s", SDL_GetError());
        return 1;
    }

    const std::uint32_t window_w = 1000;
    const std::uint32_t window_h = 600;
    
    SDL_Window *window = SDL_CreateWindow("Mattgic", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, window_w, window_h, 0);

    if (window == NULL)
    {
        SDL_Log("Unable to create window: %s", SDL_GetError());
        return 1;
    }

    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, 0);

    if (renderer == NULL)
    {
        SDL_Log("Unable to create renderer: %s", SDL_GetError());
        return 1;
    }

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);

    matt::Perlin pn;
    const double zoom = 2.6;
    double res;
    
    for (std::uint32_t i = 0; i < window_w; i++)
	for (std::uint32_t j = 0; j < window_h; j++)
	{
	    res = pn.noise(i*zoom/window_w, j*zoom/window_h);

	    // Make water more likely near edges
	    res *= 1-(sqrt(pow((window_w/2.0)-i, 2) + pow((window_h/2.0)-j, 2)) / sqrt(pow(window_w, 2)+pow(window_h, 2)));

	    if (res > 0.57)
		SDL_SetRenderDrawColor(renderer, 0, 150, 0, 255);
	    else if (res > 0.40)
		SDL_SetRenderDrawColor(renderer, 50, 200, 50, 255);
	    else
		SDL_SetRenderDrawColor(renderer, 0, 0, 150, 255);
		
	    SDL_RenderDrawPoint(renderer, i, j);
	}

    SDL_RenderPresent(renderer);

    int x;

    std::cin >> x;

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}
